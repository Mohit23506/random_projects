<!-- Begin Page Content -->
<div class="container-fluid">
  <div class="row">
    <table class="table table-striped table-bordered table-hover table-responsive" id="dataTable">
      <thead class="thead-dark">
        <th>ID</th>
        <th>Customer Name</th>
        <th>Email</th>
        <th>Order Details</th>
        <th>Status</th>
        <th>Ordered On</th>
        <th>Action</th>
      </thead>
      <tbody>
        <?php if(count($orderData) < 1) { ?>
          <tr>
            <td class="text-danger" colspan="7">No data found</td>
          </tr>
        <?php } else { $i=1; foreach($orderData as $orderDataRow){ ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $orderDataRow->fullName; ?></td>
            <td><?php echo $orderDataRow->user_email; ?></td>
            <td><?php echo $orderDataRow->order_message; ?></td>
            <td><?php
            switch ($orderDataRow->order_status) {
              case 1:
              echo "<span class='text-success'>Accepted</span>";
              break;

              case 2:
              echo "<span class='text-danger'>Rejected</span>";
              break;
              
              default:
              echo "<a href='javascript:void(0);' class='action' data-action='accept' data-orderid='".$orderDataRow->order_id."' data-toggle='tooltip' title='Accept' ><i class='fa fa-check'></i></a> &nbsp; <a href='javascript:void(0);' class='action' data-action='reject' data-orderid='".$orderDataRow->order_id."' data-toggle='tooltip' title='Reject' ><i class='fa fa-times'></i></a>";
              break;
            }
            ?></td>
            <td><?php echo date('d-m-Y h:i:s', strtotime($orderDataRow->order_createdOn)); ?></td>
            <td>
              <a href="javascript:void(0);" data-href="<?php echo base_url('Orders/delete/'.$orderDataRow->order_id.'/'.$orderDataRow->order_status); ?>" data-toggle="tooltip" title="delete" class="deleteItem">
                <i class="fas fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php  $i++; } } ?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- /.container-fluid -->

  <script>
    $('.action').each(function(i,e){
      $(this).on('click',function(){
        var order_id = $(this).data('orderid');
        var action = $(this).data('action');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes !'
        }).then((result) => {
          if (result.value) {
            // Swal.fire(
            //   'Deleted!',
            //   'Your file has been deleted.',
            //   'success'
            //   )
            window.location = "<?php echo base_url('Orders/action/')?>"+order_id+'/'+action;
            // console.log("<?php echo base_url('Orders/action/')?>"+order_id+'/'+action);
          }
        })
      });
    });
  </script>