<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- adding a button to add menu items -->
  <div class="row col-md-12">
    <a href="<?php echo base_url('Restaurent/addMenuItem'); ?>" data-toggle="tooltip" title="Add menu items">
      <i class="fa fa-plus-circle"></i>
      Add Items To Menu
    </a>
  </div>

  <?php if(count($menuItems) < 1) { ?>
    <div class="row col-md-12 text-danger">
      <marquee behavior="alternate" direction="">There is no items in menu</marquee>
    </div>
  <?php } else { ?>
    <br>
    <div class="row">
      <table class="table table-striped table-bordered table-hover table-responsive" id="dataTable">
        <thead class="thead-dark">
          <th>ID</th>
          <th>Name</th>
          <th>Description</th>
          <th>Type</th>
          <th>Price</th>
          <th>Created On</th>
          <th>Action</th>
        </thead>
        <tbody>
          <?php $i=1; foreach ($menuItems as $menuItemsRow) { ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $menuItemsRow->menu_name; ?></td>
              <td><?php echo $menuItemsRow->menu_description; ?></td>
              <td><?php echo ($menuItemsRow->menu_type == 1)?'Non-Veg':'Veg'; ?></td>
              <td><?php echo $menuItemsRow->menu_price; ?></td>
              <td><?php echo date('d-m-Y h:i:s',strtotime($menuItemsRow->menu_createdOn)); ?></td>
              <td>
                <a href="<?php echo base_url('Restaurent/editItem/'.$menuItemsRow->menu_id); ?>" class="" id="" data-toggle="tooltip" title="Edit">
                  <i class="fas fa-edit"></i>
                </a>
                &nbsp;
                <a href="javascript:void(0);" data-href="<?php echo base_url('Restaurent/deleteItem/'.$menuItemsRow->menu_id); ?>" class="deleteItem" id="" data-toggle="tooltip" title="Delete">
                  <i class="fas fa-trash-alt"></i>
                </a>
              </td>
            </tr>
            <?php $i++; } } ?>
          </tbody>
        </table>
      </div>

    </div>
    <!-- /.container-fluid -->
