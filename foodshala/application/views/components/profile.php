<!-- <div class="container"> -->
	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4 row">
								<div class="col-md-3 float-left">Review Account Details!</div>
								<div class="col-md-9 float-right text-danger">* All fields are mandatory</div>
							</h1>
						</div>
						<form class="user" method="post" enctype="multipart/form-data" action="">

							<div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<input required type="text" class="form-control form-control-user" id="user_fname" name="user_fname" placeholder="First Name" value="<?php echo (set_value('user_fname'))?set_value('user_fname'):$userData->user_fname; ?>">
									<?php echo form_error('user_fname'); ?>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-user" id="user_lname" name="user_lname" placeholder="Last Name" value="<?php echo (set_value('user_lname'))?set_value('user_lname'):$userData->user_lname; ?>">
									<?php echo form_error('user_lname'); ?>
								</div>
							</div>

							<div class="form-group">
								<input required type="email" class="form-control form-control-user" id="user_email" name="user_email" placeholder="Email Address" value="<?php echo (set_value('user_email'))?set_value('user_email'):$userData->user_email; ?>">
								<?php echo form_error('user_email'); ?>
							</div>

							<div class="form-group">
								<input required type="number" class="form-control form-control-user" id="user_mobile" name="user_mobile" placeholder="Mobile Number" value="<?php echo (set_value('user_mobile'))?set_value('user_mobile'):$userData->user_mobile; ?>">
								<?php echo form_error('user_mobile'); ?>
							</div>

							<?php if($userType == '1') { ?>
								<div class="form-group" id="restaurant_name">
									<input type="text" class="form-control form-control-user" id="user_restaurantName" name="user_restaurantName" placeholder="Enter Restaurant Name" value="<?php echo (set_value('user_restaurantName'))?set_value('user_restaurantName'):$userData->user_restaurantName; ?>"" required>
									<?php echo form_error('user_restaurantName'); ?>
								</div>
							<?php } else { ?>

								<div class="form-group row" id="user_preference">
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="vegSelection" name="user_preference" class="custom-control-input" value="0" <?php echo ($userData->user_preference == 0)?'checked':'';?>>
										<label class="custom-control-label" for="vegSelection">Veg</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="non-vegSelection" name="user_preference" class="custom-control-input" value="1" <?php echo ($userData->user_preference == 1)?'checked':'';?>>
										<label class="custom-control-label" for="non-vegSelection">Non-Veg</label>
									</div>
									<?php echo form_error('user_preference'); ?>
								</div>
							<?php } ?>

							<div class="form-group">
								<textarea name="user_address" placeholder="Enter Address" id="user_address" cols="" rows="" class="form-control form-control-user" required><?php echo (set_value('user_address'))?set_value('user_address'):$userData->user_address; ?></textarea>
								<?php echo form_error('user_address'); ?>
							</div>

							<div class="form-group">
								<select name="user_state" id="user_state" class="custom-select2 form-control">
									<option value="">--Select State--</option>
									<?php foreach($states as $statesRow){ ?>
										<option value="<?php echo $statesRow->sid; ?>" <?php echo ((set_value('user_state'))?set_value('user_state'):$userData->user_state == $statesRow->sid)?'selected':'';?>><?php echo $statesRow->stateName; ?></option>
									<?php } ?>
								</select>
								<?php echo form_error('user_state'); ?>
							</div>

							<div class="form-group">
								<select name="user_district" id="user_district" class="custom-select2 form-control" required="">
									<option value="">--Select District--</option>
								</select>
								<?php echo form_error('user_district'); ?>
							</div>

							<!-- <div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<input type="password" class="form-control form-control-user" id="user_password" name="user_password" placeholder="Password" value="<?php echo set_value('user_password'); ?>">
									<?php echo form_error('user_password'); ?>
								</div>
								
								<div class="col-sm-6">
									<input type="password" class="form-control form-control-user" id="repeat_password" name="repeat_password" placeholder="Repeat Password" value="<?php echo set_value('repeat_password'); ?>">
									<?php echo form_error('repeat_password'); ?>
								</div>
							</div> -->

							<div class="form-group custom-file" data-toggle="tooltip" title="Profile Pic">
								<input type="file" class="form-control form-control-user custom-file-input" name="user_profilePic" id="user_profilePic" accept="image/*">
								<label class="custom-file-label" for="customFile">Choose file</label>
							</div>

							<?php if(!empty($userData->user_profilePic)){ ?>
								<div class="row col-md-12">
									<img src="<?php echo ($this->session->userdata('userLoginData')->user_profilePic)?base_url('datafiles/'.$this->session->userdata('userLoginData')->user_profilePic):base_url('datafiles/user.png');?>" alt="Profile Pic" class="" id="" width='100' height='100' style="border-radius:50%;">
								</div>
								<br>
							<?php } ?>

							<button class="btn btn-primary btn-user col-md-3 col-sm-6 col-xs-12" type="submit">Update</button>
							<a href="<?php echo base_url('Dashboard');?>" class="btn btn-danger btn-user col-md-3 col-sm-6 col-xs-12" type="">Cancel</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- </div> -->
	<script>
		$(document).ready(function(){
    // if state is selected, due to form validation
    $("#user_state").on('change',function(){
    	getDistricts($(this).val());
    });

    <?php if($userData->user_state){ ?>
    	$("#user_state").trigger('change');
    	getDistricts(<?php echo (set_value('user_state'))?set_value('user_state'):$userData->user_state ?>, <?php echo (set_value('user_district'))?set_value('user_district'):$userData->user_district; ?>);
    	<?php } ?>;
    });
  </script>