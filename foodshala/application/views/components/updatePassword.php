
<form action="" method="post" id="passwordForm">
	<div class="form-group">
		<input required type="password" class="form-control form-control-user" id="Old_password" name="Old_password" placeholder="Old Password">
	</div>

	<div class="form-group">
		<input required type="password" class="form-control form-control-user" id="New_password" name="New_password" placeholder="New Password">
	</div>

	<div class="form-group">
		<input required type="password" class="form-control form-control-user" id="Confirm_password" name="Confirm_password" placeholder="Confirm Password">
	</div>

	<button class="btn btn-primary btn-user col-md-3 col-sm-6 col-xs-12" type="submit">Update</button>
	<a href="<?php echo base_url('Dashboard');?>" class="btn btn-danger btn-user col-md-3 col-sm-6 col-xs-12" type="">Cancel</a>
</form>

<script>
	$(document).ready(function(){
		$('#passwordForm').on('submit',function(e){
			e.preventDefault();
			var formData = $('#passwordForm').serialize();
			// console.log(formData);
			var Old_password     = $('#Old_password').val();
			var New_password     = $('#New_password').val();
			var Confirm_password = $('#Confirm_password').val();
			if (Old_password.length<1 || New_password.length<1 || Confirm_password.length<1)
			{
				Swal.fire({
					icon: 'error',
					title: 'Error',
					html: 'All fields are required'
				});
				return false;
			}

			else if(New_password != Confirm_password)
			{
				// if new password and confirm password don't match
				Swal.fire({
					icon: 'error',
					title: 'Error',
					html: 'New password is not matched with confirmed password'
				});
				return false;
			}

			else
			{
				$.ajax({
					url :"<?php echo base_url();?>Ajax/updatePassword/",
					type: "POST",
					data: formData,
					success: function( result )
					{
						result = JSON.parse(result);
						if(result.status == 200)
						{
							Swal.fire({
								icon : result.icon,
								title: result.title,
								html : result.message
							});
							// after showing alert, logout user and then enter with the new password
							let timerInterval
							Swal.fire({
								title: 'Logging out!',
								html: 'Please login with your updated password.',
								timer: 2000,
								timerProgressBar: true,
								onBeforeOpen: () => {
									Swal.showLoading()
									timerInterval = setInterval(() => {
										const content = Swal.getContent()
										if (content) {
											const b = content.querySelector('b')
											if (b) {
												b.textContent = Swal.getTimerLeft()
											}
										}
									}, 100)
								},
								onClose: () => {
									clearInterval(timerInterval);
									window.location = "<?php echo base_url('Site/login'); ?>"
								}
							}).then((result) => {
								/* Read more about handling dismissals below */
								if (result.dismiss === Swal.DismissReason.timer) {
									console.log('I was closed by the timer')
								}
							})
						}
						else
						{
							Swal.fire({
								icon : result.icon,
								title: result.title,
								html : result.message
							});
						}
					},

				});
			}
		});
	});
</script>