<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Welcome To FoodShala</h1>
    <a href="<?php echo base_url('Site/login'); ?>" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-user fa-sm text-white-50"></i> Login / Sign-Up</a>
  </div>

  <?php if(count($restaurants) < 1) { ?>
    <div class="row col-md-12">
      <img src="<?php echo base_url('datafiles/comingSoon.jpg'); ?>" alt="">
    </div>
  <?php } else { ?>
    <div class="row col-md-12 mb-4">
      Select restaurent's menu to order
    </div>
    <div class="row">
      <?php foreach ($restaurants as $restaurantsRow) { ?>
        <div class="col-md-3 pb-1">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="<?php echo ($restaurantsRow->user_profilePic)?base_url('datafiles/'.$restaurantsRow->user_profilePic):base_url('datafiles/res_default.jpg');?>" alt="Simulation Game" style="max-height: 164px;">
            <div class="card-body" style="min-height: 60px;">
              <p class="card-text">
                <h5>
                  <?php echo ucfirst($restaurantsRow->user_restaurantName) ;?>
                  <a href="javascript:void(0);" data-toggle="tooltip" title="Menu/Order" class="float-right"><i class="fa fa-tasks" data-rid="<?php echo $restaurantsRow->user_id?>"></i></a>
                </h5>
              </p>
              <h5 class="card-title"><?php echo $restaurantsRow->user_address.'<br>'.$restaurantsRow->districtName.', '.$restaurantsRow->stateName; ?></h5><br>
              <!-- <p class="card-text"><?php echo count($gameData);?> Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
            </div>
          </div>
        </div>
      <?php } } ?>
    </div>

  </div>
        <!-- /.container-fluid -->