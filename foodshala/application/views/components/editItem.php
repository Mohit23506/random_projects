<!-- <div class="container"> -->
	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<?php if(count($item) < 1){ ?>
						<div class="row col-md-12 text-danger">
							<marquee behavior="alternate" direction="">Invalid Selection</marquee>
						</div>
					<?php } else { ?>
						<div class="p-5">
							<div class="text-center">
								<h1 class="h4 text-gray-900 mb-4 row">
									<div class="col-md-3 float-left">Edit Menu Item!</div>
									<div class="col-md-9 float-right text-danger">* All fields are mandatory</div>
								</h1>
							</div>
							<form class="user" method="post" enctype="multipart/form-data" action="">

								<div class="form-group">
									<input required type="text" class="form-control form-control-user" id="menu_name" name="menu_name" placeholder="Enter Item Name" value="<?php echo (set_value('menu_name'))?set_value('menu_name'):$item[0]->menu_name; ?>">
									<?php echo form_error('menu_name'); ?>
								</div>

								<div class="form-group">
									<textarea name="menu_description" id="menu_description" placeholder="Enter Description" class="form-control form-control-user" required=""><?php echo (set_value('menu_description'))?set_value('menu_description'):$item[0]->menu_description; ?></textarea>
									<?php echo form_error('menu_description'); ?>
								</div>

								<div class="form-group row" id="menu_type">
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="vegSelection" checked="" name="menu_type" class="custom-control-input" value="0">
										<label class="custom-control-label" for="vegSelection">Veg</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="non-vegSelection" name="menu_type" class="custom-control-input" value="1" <?php echo (set_value('menu_type') ==1 || $item[0]->menu_type == 1)?'checked':''?>>
										<label class="custom-control-label" for="non-vegSelection">Non-Veg</label>
									</div>
									<?php echo form_error('menu_type'); ?>
								</div>

								<div class="form-group">
									<input required type="number" class="form-control form-control-user" id="menu_price" name="menu_price" placeholder="Enter Item Price" value="<?php echo (set_value('menu_price'))?set_value('menu_price'):$item[0]->menu_price; ?>">
									<?php echo form_error('menu_price'); ?>
								</div>

								<button class="btn btn-primary btn-user col-md-3 col-sm-6 col-xs-12" type="submit">Add</button>
								<a href="<?php echo base_url('Restaurent');?>" class="btn btn-danger btn-user col-md-3 col-sm-6 col-xs-12" type="">Cancel</a>
							</form>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

