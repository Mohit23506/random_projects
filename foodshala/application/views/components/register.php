<div class="container bg-gradient-primary">
	<div class="card o-hidden border-0 shadow-lg my-5">
		<div class="card-body p-0">
			<!-- Nested Row within Card Body -->
			<div class="row">
				<div class="col-lg-7">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
							<div class="text-danger">* All fields are mandatory</div>
						</div>
						<form class="user" method="post" enctype="multipart/form-data" action="">
							<!-- select filter type -->
							<div class="form-group row">
								<div class="custom-control custom-radio custom-control-inline">
									<input required type="radio" id="customerSelection" checked="" name="user_role" class="custom-control-input" value="0">
									<label class="custom-control-label" for="customerSelection">Customer</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input required type="radio" id="restaurantSelection" name="user_role" class="custom-control-input" value="1" <?php echo ($userType == '1')?'checked':''; ?>>
									<label class="custom-control-label" for="restaurantSelection">Restaurants</label>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<input required type="text" class="form-control form-control-user" id="user_fname" name="user_fname" placeholder="First Name" value="<?php echo set_value('user_fname'); ?>">
									<?php echo form_error('user_fname'); ?>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-user" id="user_lname" name="user_lname" placeholder="Last Name" value="<?php echo set_value('user_lname'); ?>">
									<?php echo form_error('user_lname'); ?>
								</div>
							</div>

							<div class="form-group">
								<input required type="email" class="form-control form-control-user" id="user_email" name="user_email" placeholder="Email Address" value="<?php echo set_value('user_email'); ?>">
								<?php echo form_error('user_email'); ?>
							</div>

							<div class="form-group">
								<input required type="number" class="form-control form-control-user" id="user_mobile" name="user_mobile" placeholder="Mobile Number" value="<?php echo set_value('user_mobile'); ?>">
								<?php echo form_error('user_mobile'); ?>
							</div>

							<?php if($userType == '1') { ?>
								<div class="form-group" id="restaurant_name">
									<input type="text" class="form-control form-control-user" id="user_restaurantName" name="user_restaurantName" placeholder="Enter Restaurant Name" value="<?php echo set_value('user_restaurantName'); ?>" required>
									<?php echo form_error('user_restaurantName'); ?>
								</div>
							<?php } else { ?>

								<div class="form-group row" id="user_preference">
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="vegSelection" checked="" name="user_preference" class="custom-control-input" value="0">
										<label class="custom-control-label" for="vegSelection">Veg</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input required type="radio" id="non-vegSelection" name="user_preference" class="custom-control-input" value="1">
										<label class="custom-control-label" for="non-vegSelection">Non-Veg</label>
									</div>
									<?php echo form_error('user_preference'); ?>
								</div>
							<?php } ?>

							<div class="form-group">
								<textarea name="user_address" placeholder="Enter Address" id="user_address" cols="" rows="" class="form-control form-control-user" required><?php echo set_value('user_address'); ?></textarea>
								<?php echo form_error('user_address'); ?>
							</div>

							<div class="form-group">
								<select name="user_state" id="user_state" class="custom-select2 form-control">
									<option value="">--Select State--</option>
									<?php foreach($states as $statesRow){ ?>
										<option value="<?php echo $statesRow->sid; ?>" <?php echo (set_value('user_state') == $statesRow->sid)?'selected':'';?>><?php echo $statesRow->stateName; ?></option>
									<?php } ?>
								</select>
								<?php echo form_error('user_state'); ?>
							</div>

							<div class="form-group">
								<select name="user_district" id="user_district" class="custom-select2 form-control" required="">
									<option value="">--Select District--</option>
								</select>
								<?php echo form_error('user_district'); ?>
							</div>

							<div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<input required type="password" class="form-control form-control-user" id="user_password" name="user_password" placeholder="Password" value="<?php echo set_value('user_password'); ?>">
									<?php echo form_error('user_password'); ?>
								</div>
								<div class="col-sm-6">
									<input required type="password" class="form-control form-control-user" id="repeat_password" name="repeat_password" placeholder="Repeat Password" value="<?php echo set_value('repeat_password'); ?>">
									<?php echo form_error('repeat_password'); ?>
								</div>
							</div>

							<div class="form-group custom-file" data-toggle="tooltip" title="Profile Pic">
								<input type="file" class="form-control form-control-user custom-file-input" name="user_profilePic" id="user_profilePic" accept="image/*">
								<label class="custom-file-label" for="customFile">Choose file</label>
							</div>

							<button class="btn btn-primary btn-user btn-block" type="submit">Register Account</button>
						</form>
					</div>
				</div>

				<div class="col-lg-5 d-lg-block mt-4">
					<div class="text-center d-none">
						<a class="small" href="<?php echo base_url('Site/resetPassword');?>">Forgot Password?</a>
					</div>
					<div class="text-center">
						<a class="small" href="<?php echo base_url('Site/login');?>">Already have an account? Login!</a>
					</div>
					<div class="text-center">
						<a href="<?php echo base_url(); ?>">← Back to Home</a>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
<script>
	$(document).ready(function(){
		$('input[type="radio"][name="user_role"]').on('change',function(){
			var type = $(this).val();
			window.location = '<?php echo base_url('Site/register/');?>'+type;
		});
    // if state is selected, due to form validation
    $("#user_state").on('change',function(){
    	getDistricts($(this).val());
    });

    <?php if(set_value('user_state')){ ?>
    	$("#user_state").trigger('change');
    	getDistricts(<?php echo set_value('user_state');?>, <?php echo set_value('user_district'); ?>);
    	<?php } ?>;
    });
  </script>