<!-- Begin Page Content -->
<div class="container-fluid">
  <div class="row">
    <table class="table table-striped table-bordered table-hover table-responsive" id="dataTable">
      <thead class="thead-dark">
        <th>ID</th>
        <th>Restaurant Name</th>
        <th>Order Details</th>
        <th>Status</th>
        <th>Created On</th>
        <th>Action</th>
      </thead>
      <tbody>
        <?php if(count($orderData) < 1) { ?>
          <tr>
            <td class="text-danger" colspan="6">No data found</td>
          </tr>
        <?php } else { $i=1; foreach($orderData as $orderDataRow){ ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $orderDataRow->user_restaurantName; ?></td>
            <td><?php echo $orderDataRow->order_message; ?></td>
            <td><?php
            switch ($orderDataRow->order_status) {
              case 1:
              echo "<span class='text-success'>Accepted</span>";
              break;

              case 2:
              echo "<span class='text-danger'>Rejected</span>";
              break;
              
              default:
              echo "<span>Pending</span>";
              break;
            }
            ?></td>
            <td><?php echo date('d-m-Y h:i:s', strtotime($orderDataRow->order_createdOn)); ?></td>
            <td>
              <a href="javascript:void(0);" data-href="<?php echo base_url('Orders/delete/'.$orderDataRow->order_id.'/'.$orderDataRow->order_status); ?>" data-toggle="tooltip" title="delete" class="deleteItem">
                <i class="fas fa-trash-alt"></i>
            </a>
          </td>
          </tr>
          <?php  $i++; } } ?>
        </tbody>
      </table>
    </div>
  </div>
        <!-- /.container-fluid -->