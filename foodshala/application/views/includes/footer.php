</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Your Website 2019</span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Are you sure want to end your current session?</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <?php if($this->session->userdata('userLoginData')->user_role == 1) { ?>
          <a class="btn btn-primary" href="<?php echo base_url('Restaurent/logout'); ?>">Logout</a>
        <?php } else { ?>
          <a class="btn btn-primary" href="<?php echo base_url('Customer/logout'); ?>">Logout</a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('common/vendor/bootstrap/js/bootstrap.bundle.min.js?v=').file_version_cs; ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('common/vendor/jquery-easing/jquery.easing.min.js?v=').file_version_cs; ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('common/js/sb-admin-2.min.js?v=').file_version_cs; ?>"></script>

<!-- Page level plugins -->
<!-- Page level custom scripts -->
<!-- 
  <script src="<?php echo base_url('common/vendor/chart.js/Chart.min.js?v=').file_version_cs; ?>"></script>
  <script src="<?php echo base_url('common/js/demo/chart-area-demo.js?v=').file_version_cs; ?>"></script>
  <script src="<?php echo base_url('common/js/demo/chart-pie-demo.js?v=').file_version_cs; ?>"></script>
-->
<!-- for searchable dropdown -->
<link href="<?php echo base_url('common/css/select2.min.css?v=').file_version_cs;?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('common/js/select2.min.js?v=').file_version_cs;?>"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url('common/vendor/datatables/jquery.dataTables.min.js?v=').file_version_cs; ?>"></script>
<script src="<?php echo base_url('common/vendor/datatables/dataTables.bootstrap4.min.js?v=').file_version_cs; ?>"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url('common/js/demo/datatables-demo.js?v=').file_version_cs; ?>"></script>

<!-- loading error message -->
<?php $this->load->view('components/trErAlert'); ?>
<script>
  $(document).ready(function(){
    // console.log('page is loaded');
    $('.deleteItem').each(function(i,e){
      $(this).on('click',function(){
        var redirectUrl = $(this).data('href');
        Swal.fire({
          title: 'Are you sure want to delete?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            // Swal.fire(
            //   'Deleted!',
            //   'Your file has been deleted.',
            //   'success'
            //   )
            window.location = redirectUrl;
          }
        })
      });
    });

    // while user click on task icon
    $('.fa-tasks').each(function(e){
      $(this).on('click',function(){
        // this is nothing but the user_id
        var restaurantId = $(this).data('rid');
        // trigger ajax and fetch the menu item data
        $.ajax({
          url :"<?php echo base_url();?>Ajax/getMenuItems/"+restaurantId,
          type: "POST",
          success: function( result )
          {
            result = JSON.parse(result);
            if(result.status == 200)
            {
              var menuItems = result.data;
              var orderForm = "<form class='form' id='orderFormData' method='post'>";
              console.log(menuItems);
              // create a form and show html in the popup to place order
              $.each(menuItems, function(i,e){
                var menu_name = '';
                if(menuItems[i].menu_type == 1)
                {
                  //this is non-veg menuItems[i].menu_name
                  menu_name = '<span class="text-danger" data-toggle="tooltip" title="'+menuItems[i].menu_description+'">'+menuItems[i].menu_name+'</span>';
                }
                else
                {
                  menu_name = '<span class="text-success" data-toggle="tooltip" title="'+menuItems[i].menu_description+'">'+menuItems[i].menu_name+'</span>';
                }

                orderForm += '<div class="form-row"><input type="hidden" name="restaurantId" value="'+menuItems[i].menu_user_id+'"> <label for="'+menuItems[i].menu_id+'">'+menu_name+'</label> &nbsp; <input type="hidden" name="itemPrice[]" value="'+menuItems[i].menu_price+'"> <input type="hidden" name="itemName[]" value="'+menuItems[i].menu_name+'"> <input name="itemQuantity[]" type="range" class="custom-range" min="0" max="3" step="1" data-price="'+menuItems[i].menu_price+'" id="'+menuItems[i].menu_id+'"style="width:20%; height:calc(1rem + 1rem);" value="0" title="Max 3 order at a time"><input data-toggle="tooltip" title="Price for the selected items" id="itemPrice_'+menuItems[i].menu_id+'" class="form-control form-control-user" data-price="'+menuItems[i].menu_price+'" value="'+menuItems[i].menu_price+'" disabled style="max-width: 45%;"></div>';
              });
              orderForm += '<div class="row col-md-12" id="totalItemPrice"></div><br><div class="row col-md-12 ml-4"> <button class="btn btn-primary" id="submitOrder" type="submit">Order</button> &nbsp; <button class="btn btn-outline-danger closeModel" type="button">Cancel</button></div></form>';
              Swal.fire({
                // icon: result.icon,
                title            : result.title,
                html             : orderForm,
                showConfirmButton: false,
                customClass      : 'swal-size-sm',
                footer           : result.footer,
                // showCancelButton : true,
                // cancelButtonColor: '#e74a3b',
              });
              closeModel(); calculateItemPrice(); orderFormDataStatus();
            }
            else
            {
              Swal.fire({
                icon: result.icon,
                title: result.title,
                html: result.message
              });
            }
          },
        });
      });
    });
    // show image model on click
    $('img').on('click',function(){
      if(!$(this).hasClass('noPopUp'))
      {
        var imageUrl  = $(this).attr('src');
        var effectIn  = 'animated flip faster';
        var effectOut = 'animated lightSpeedOut faster';
        // alert(this.width + 'x' + this.height);
        Swal.fire({
          // imageWidth       : 200,
          // imageHeight      : 100,
          imageUrl         : imageUrl,
          imageAlt         : 'Custom image',
          icon             : 'success',
          title            : '',
          showCancelButton : true,
          showConfirmButton: false,
          cancelButtonColor: '#e74a3b',
          showClass: {
            popup: effectIn
          },
          hideClass: {
            popup: effectOut
          }
          // html             : 'You are allowed to play <b>"'+gameName+'"</b> from <b>"'+startDate+'"</b> to <b>"'+endDate+'"</b>',
          // footer           : '<a href>Why do I have this issue?</a>'
          // footer           : ''
        });
      }
    });
  });

function calculateItemPrice()
{
  $('.custom-range').each(function(){
    $(this).on('input', function(){
      var input_id  = $(this).attr('id');
      var noOfItems = $(this).val();
      var price     = $('#itemPrice_'+input_id).data('price');
        // $(this).attr('title',noOfItems).tooltip('fixTitle').tooltip('show');
        $('#itemPrice_'+input_id).val(noOfItems+'*'+price+' = '+eval(noOfItems*price));
        totalItemPrice();
      });
  });
}

function closeModel()
{
  $('.closeModel').each(function(){
    $(this).on('click',function(){
      Swal.close();
    });
  });
}

function getDistricts(sid,did)
{
    // show districts according to state selection
    var districts = '<option value="">--select district--</option>';
    var selected  = '';

    if(!sid)
    {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        html: 'Please select state'
      });
      return false;
    }
    else
    {
      $.ajax({
        url :"<?php echo base_url();?>Ajax/getDistricts/"+sid,
        type: "POST",
        success: function( result )
        {
          result = JSON.parse(result);
          if(result.status == 200)
          {
            $.each(result.districts,function(i,e){
              if(did == result.districts[i].did)
              {
                selected = 'selected';
              }
              else
              {
                selected = '';
              }
              districts += '<option '+selected+' value="'+result.districts[i].did+'">'+result.districts[i].districtName+'</option>';
            });
            $("#user_district").html(districts);
          }
          else
          {
            Swal.fire({
              icon: result.icon,
              title: result.title,
              html: result.message
            });
          }
        },
      });
    }    
  }

  function totalItemPrice()
  {
    var totalItemPrice = 0;
    // var timestamp = new Date().getTime();
    $('.custom-range').each(function(){
      var perItemPrice = $(this).data('price');
      var quantity     = $(this).val();
      // console.log(perItemPrice + ' and '+ quantity + ' id '+$(this).attr('id'));
      totalItemPrice += Math.round(eval(perItemPrice*quantity));
    });
    $('#totalItemPrice').html('<br><b>Total:- <input type="text" readonly value="'+totalItemPrice+'" name="totalItemPrice"> </b>');
  }

  function orderFormDataStatus()
  {
    $('#orderFormData').on('submit', function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      // console.log(formData);
      $.ajax({
        url :"<?php echo base_url();?>Ajax/placeOrder/",
        type: "POST",
        data: formData,
        success: function( result )
        {
          result = JSON.parse(result);
          console.log(result);
          if(result.status == 200)
          {
            Swal.fire({
              icon: result.icon,
              title: result.title,
              html: result.message
            });
          }
          else
          {
            Swal.fire({
              icon: result.icon,
              title: result.title,
              html: result.message
            });
            if(result.data)
            {
              window.location = "<?php echo base_url('Site/login/');?>"+result.data+'/'+result.rid;
            }
          }
        },
      });
    });
  }
</script>
</body>

</html>