<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>FoodShala</title>

	<!-- Custom fonts for this template-->
	<link href="<?php echo base_url('common/vendor/fontawesome-free/css/all.min.css?v=').file_version_cs; ?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?php echo base_url('common/css/sb-admin-2.min.css?v=').file_version_cs; ?>" rel="stylesheet">

	<!-- adding sweetalert library -->
	<link href="<?php echo base_url('common/sweetalert/sweetalert2.min.css?v=').file_version_cs;?>" rel="stylesheet" type="text/css">

	<link href="<?php echo base_url('common/sweetalert/animate.min.css?v=').file_version_cs;?>" rel="stylesheet" type="text/css">

	<script src="<?php echo base_url('common/sweetalert/sweetalert2.all.min.js?v=').file_version_cs;?>"></script>
	<style>
		.swal2-container.swal2-backdrop-show{
			background: #00000066;
		}
		.swal-size-sm
		{
			width: auto;
		}
		.dot-success {
			height          : 15px;
			width           : 15px;
			background-color: #bbb;
			border-radius   : 50%;
			display         : inline-block;
			background      : #1cc88a;
		}
		.dot-danger {
			height          : 15px;
			width           : 15px;
			background-color: #bbb;
			border-radius   : 50%;
			display         : inline-block;
			background      : #e74a3b;
		}
		.swal2-footer
		{
			font-size  : 11px;
			font-weight: bold;
		}
		.pre-loader
		{
			background         : url(../foodshala/datafiles/new-loader.gif) no-repeat #00000066;
			background-position: center center;
			background-size    : 13%;
			position           : fixed;
			left               : 0;
			top                : 0;
			width              : 100%;
			height             : 100%;
			z-index            : 12345;
			display            : none;
		}
	</style>
	<script src="<?php echo base_url('common/vendor/jquery/jquery.min.js?v=').file_version_cs; ?>"></script>
	<!-- adding datatable plugins -->
	<link href="<?php echo base_url('common/vendor/datatables/dataTables.bootstrap4.min.css?v=').file_version_cs ?>" rel="stylesheet">

	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();

		// show pre-loader when ajax starts
		$(document).ajaxStart(function(){
			$('#pre-loader').show();
		});
		// hide pre-loader when ajax starts
		$(document).ajaxComplete(function(){
			$('#pre-loader').hide();
			$('[data-toggle="tooltip"]').tooltip();
		});
		
	});
</script>
</head>

<body id="page-top">
<div class="pre-loader" id="pre-loader"></div>
	<!-- Page Wrapper -->
	<div id="wrapper">