<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonModel extends CI_Model {

  // public function __construct()
  // {
  //  parent::__construct();
  //  $this->load->model('CommonModel');
  // }

  // finding the count of num_rows
  public function findCount($tableName=NULL,$where=NULL)
  {
    $this->db->where($where);
    $result = $this->db->get($tableName);
    // echo "<pre>"; print_r($this->db->last_query()); exit();
    // echo "<pre>"; print_r($result->num_rows()); exit();
    return $result->num_rows();
  }

//fetch record of login Enterprise
  public function fetchRecords($tableName=NULL,$where=NULL,$columnName=NULL,$order=NULL,$limit=NULL,$printQuery=NULL)
  {
    // die('in modal');
    if($columnName)
    {
      $this->db->select($columnName);
    }
    if($order)
    {
      $this->db->order_by($order);
    }
    if($limit)
    {
      $limit = explode(',',$limit);
      $this->db->limit($limit[1],$limit[0]);
    }
    $this->db->from($tableName);
    if($where)
    {
      $this->db->where($where);
    }
    $query  = $this->db->get();
    $result = $query->result();
    if($printQuery)
    {
      print_r($this->db->last_query()); // exit();
    }
    return $result;
  }

  //insert users of Enterprise/SubEnterprise
  public  function insert($tableName=NULL,$data=NULL,$check=NULL)
  {
    if($check)
    {
      // check for duplicate records
      $this->db->where($check);
      $duplicateQuery  = $this->db->get($tableName);
      $duplicateResult = $duplicateQuery->result();
      if(count($duplicateResult) > 0)
      {
        return 'duplicate';
      }
      else
      {
        $this->db->insert($tableName,$data);
        // print_r($this->db->last_query());exit();
        $userId = $this->db->insert_id();
        return $userId;
      }
    }
    else
    {
      $this->db->insert($tableName,$data);
      // print_r($this->db->last_query());exit();
      $userId = $this->db->insert_id();
      return $userId;
    }
  }

//edit password
  public function editPassword($id)
  {   
    $this->db->select('*');
    $this->db->from('GAME_USER_AUTHENTICATION');
    $this->db->where('Auth_userid',$id);  
    $query=$this->db->get();
    $result = $query->result();
    return $result;
    /*print_r($this->db->last_query());
    die(' here');*/
  }

//Update Password
  public function updatePassword($id=NULL,$password=NULL)
  {
    $this->db->where('Auth_userid',$id);
    $this->db->update('GAME_USER_AUTHENTICATION',$password);
    //print_r($this->db->last_query());exit();
    return $this->db->last_query();
  }

//Update User Details
  public function update($tableName=NULL,$id=NULL,$data=NULL,$check=NULL,$checkCol=NULL)
  {
    if($checkCol)
    {
      $this->db->where("$checkCol !=", $id);
    }
    $this->db->group_start();
    $this->db->or_where($check);
    $this->db->group_end();
    $result = $this->db->get($tableName)->result();
    // echo "$checkCol<pre>"; print_r($this->db->last_query()); print_r($result); exit();
    if(count($result) > 0)
    {
      $return = 'duplicate';
    }
    else
    {
      $this->db->where($checkCol,$id);
      $this->db->update($tableName,$data);
      $return = 'update';
    }
    //print_r($this->db->last_query());exit();
    return $return;
  }

  // pass query as an argument to execute
  public function executeQuery($query=NULL,$noReturn=NULL)
  {
    $query  = $this->db->query($query);
    // echo "<pre>"; print_r($this->db->last_query()); print_r($result); exit();
    if($noReturn != 'noReturn')
    {
     $result = $query->result();
     return $result;
   }
 }

 public function getMenuItems($tableName=NULL,$joinTable=NULL,$columnName=NULL,$joinColCond=NULL,$where=NULL, $order=NULL)
 {
  $this->db->select($columnName);
  $this->db->from($tableName);
  $this->db->join($joinTable, $joinColCond,'LEFT');
  $this->db->where($where);
  if($order)
  {
    $this->db->order_by($order);
  }
  $query = $this->db->get();
  $result = $query->result();
  // print_r($this->db->last_query());
  return $result;
}

//random password generate for user
function random_password() 
{
 $alphabet     = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
 $password     = array(); 
 $alpha_length = strlen($alphabet) - 1; 
 for ($i = 0; $i < 8; $i++) 
 {
  $n = rand(0, $alpha_length);
  $password[] = $alphabet[$n];
}
return implode($password); 
}

 //Delete Records
public function deleteRecords($tableName=NULL,$where=NULL)
{
 $this->db->where($where);
 $this->db->delete($tableName);
    // print_r($this->db->last_query()); exit();
}

 //Update Records
public function updateRecords($tableName=NULL,$data=NULL,$where=NULL,$printQuery=NULL)
{
  $this->db->where($where);
  $affectedRows = $this->db->update($tableName,$data);
  if($printQuery)
  {
    print_r($this->db->last_query());
    echo '<br>'.$affectedRows;
  }
  return $affectedRows;
}

public function softDelete($tableName=NULL,$data=NULL,$where=NULL)
{
  $this->db->where($where);
  $query = $this->db->get($tableName);
  $result = $query->result();
  if(count($result) < 1)
  {
    $this->session->set_flashdata('er_msg', 'Record does not exist, or deleted. Or, user might have cancelled the order.');
    return 'not_exist';
  }
  else
  {
    $this->db->where($where);
    $affectedRows = $this->db->update($tableName,$data);
    if($affectedRows>0)
    {
      $this->session->set_flashdata('tr_msg', 'Record Deleted Successfully');
    }
    else
    {
      $this->session->set_flashdata('er_msg', 'Data Error Occured. Please try later');
    }
    // print_r($this->db->last_query()); exit();
    return $affectedRows;
  }
}

}
