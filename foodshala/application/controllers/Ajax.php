<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $userLoginData;
	public function __construct()
	{
		parent::__construct();
		$this->userLoginData = $this->session->userdata('userLoginData');
	}

	public function getDistricts($stateId=NULL)
	{
		if(empty($stateId))
		{
			die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'Please select state']));
		}

		$districts = $this->CommonModel->fetchRecords('districts', array('is_deleted' => 0, 'sid' => $stateId));
		die(json_encode(["status" => "200", 'title' => 'Success', 'icon' => 'success', 'message' => 'District fetched successfully', 'districts' => $districts]));
	}

	public function updatePassword()
	{
		// prd($this->input->post());
		$Old_password     = trim($this->input->post('Old_password'));
		$New_password     = trim($this->input->post('New_password'));
		$Confirm_password = trim($this->input->post('Confirm_password'));
		if(empty($Old_password) || empty($New_password) || empty($Confirm_password))
		{
			die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'All fields are mandatory']));
		}
		elseif($New_password != $Confirm_password)
		{
			die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'New password is not matched with confirmed password.']));
		}
		else
		{
			$userPassword = $this->CommonModel->fetchRecords('users', array('user_id' => $this->userLoginData->user_id, 'user_delete' => 0, 'user_password' => md5($Old_password)), 'user_password');
			// check that entered password is correct or not
			if(count($userPassword) > 0)
			{
				$updatePassword = $this->CommonModel->updateRecords('users', array('user_password' => md5($New_password)), array('user_id' => $this->userLoginData->user_id));
				if($updatePassword)
				{
					$this->session->unset_userdata('userLoginData');
					$this->session->set_flashdata('tr_msg', 'Password changed successfully. Please login with your new passwrod.');
					die(json_encode(["status" => "200", 'title' => 'Success', 'icon' => 'success', 'message' => 'Password updated successfully.', 'affectedRows' => $updatePassword]));
				}
				else
				{
					die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'Connection Error, Please try later.', 'affectedRows' => $updatePassword]));
				}
			}
			else
			{
				// incorrect current password is entered
				die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'Incorrect password is provided.']));
			}
		}
	}

	public function getMenuItems($user_id=NULL)
	{
		if(empty($user_id))
		{
			die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'No restaurent selected.']));
		}
		else
		{
			$itemList = $this->CommonModel->getMenuItems('menu', 'users', 'menu.*', 'users.user_id = menu.menu_user_id AND menu.menu_delete=0', array('user_id' => $user_id));
			if(count($itemList) < 1)
			{
				die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'Selected Restaurent Has No Item To Order.']));
			}
			else
			{
				die(json_encode(["status" => "200", 'title' => 'Select Items Quantity To Order', 'footer' => '<span class="dot-success"></span> &nbsp; are Veg and &nbsp; <span class="dot-danger"></span> &nbsp; are Non-Veg Items', 'icon' => 'success', 'message' => 'Please wait while loading data.', 'data' => $itemList]));
			}
		}

	}

	public function placeOrder()
	{
		// prd($this->input->post());prd($this->input->post('itemPrice'));
		$itemQuantity = $this->input->post('itemQuantity'); // of array type
		$itemPrice    = $this->input->post('itemPrice'); // of array type
		$itemName     = $this->input->post('itemName'); // of array type
		$totalPrice   = $this->input->post('totalItemPrice');
		$restaurantId = $this->input->post('restaurantId');
		$message      = array();
		$insertFlag   = false;

		for($i=0; $i<count($itemQuantity); $i++)
		{
			if($itemQuantity[$i] > 0)
			{
				$insertFlag = true;
				// if the order is placed, i.e. quantity is not 0 => n item of price, n item of price <br> Total is: totalItemPrice
				$message[] = $itemQuantity[$i].' <b>'.$itemName[$i].'</b> of price '.$itemPrice[$i];
			}
		}

		if(!$insertFlag)
		{
			die(json_encode(["status" => "201", 'title' => 'No Item Selected', 'icon' => 'error', 'message' => 'Please select at least one item to place order.', 'data' => '']));
		}
		$message[] = 'Total price is:- '.$totalPrice;
		$message   = implode('<br>',$message);
		// if user is logged in then place order, otherwise redirect to login page with data
		if($this->session->userdata('userLoginData') == NULL)
		{
			$data = base64_encode($message);
			die(json_encode(["status" => "201", 'title' => 'Session Expired', 'icon' => 'error', 'message' => 'Please Login To Place Order.', 'data' => $data, 'rid' => $restaurantId]));
		}
		else
		{
			if($this->userLoginData->user_role == 1)
			{
				die(json_encode(["status" => "201", 'title' => 'Error', 'icon' => 'error', 'message' => 'Only customer can make orders.']));
			}
			// insert data to database table and show message to user
			$insertArray = array(
				'order_from'    => $this->userLoginData->user_id,
				'order_to'      => $restaurantId,
				'order_message' => $message,
			);
			$this->CommonModel->insert('orders', $insertArray);
			die(json_encode(["status" => "200", 'title' => 'Success', 'icon' => 'success', 'message' => 'Your order has been placed successfully.']));
		}
	}

}
