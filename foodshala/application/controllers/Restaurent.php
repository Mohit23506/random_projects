<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurent extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $userLoginData;
	public function __construct()
	{
		parent::__construct();
		$this->userLoginData = $this->session->userdata('userLoginData');
		if( $this->userLoginData->user_role < 1)
		{
			redirect('Customer');
		}
	}

	public function index($orderData=NULL)
	{
		// lisging all the items of the logged in restaurent
		if($orderData)
		{
			$this->session->set_flashdata('er_msg', 'Only customer can make orders');
			redirect('Restaurent/');
		}
		$menuItems            = $this->CommonModel->fetchRecords('menu', array('menu_user_id' => $this->userLoginData->user_id, 'menu_delete' => 0));
		$content['menuItems'] = $menuItems;
		$content['subview']   = 'restaurantDashboard';
		$this->load->view('main_layout',$content);
	}

	public function addMenuItem()
	{
		// add items to restaurent menu
		$RequestMethod       = $this->input->server('REQUEST_METHOD');
		$hasValidationErrors = false;

		if($RequestMethod == 'POST')
		{
			$menu_name        = $this->input->post('menu_name');
			$menu_description = $this->input->post('menu_description');
			$menu_type        = $this->input->post('menu_type');
			$menu_price       = $this->input->post('menu_price');

			$this->form_validation->set_rules('menu_name','Name','trim|required|alpha_numeric_spaces');
			$this->form_validation->set_rules('menu_description','Description','trim|required|alpha_numeric_spaces');
			$this->form_validation->set_rules('menu_type','Type','trim|required');
			$this->form_validation->set_rules('menu_price','Price','trim|required');
			if($this->form_validation->run() == FALSE)
			{
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible mt-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>',
					'</div>');

				$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors	=	true;
				goto prepareview;
			}
			$insertArray = array(
				'menu_name'        => $menu_name,
				'menu_description' => $menu_description,
				'menu_type'        => $menu_type,
				'menu_price'       => $menu_price,
				'menu_user_id'     => $this->userLoginData->user_id,
			);
			$insertData = $this->CommonModel->insert('menu',$insertArray, array('menu_user_id' =>  $this->userLoginData->user_id, 'menu_name' => $menu_name, 'menu_delete' => 0));
			if($insertData == 'duplicate')
			{
				$this->session->set_flashdata('er_msg', 'Item Name already exist');
				redirect('Restaurent/addMenuItem');
			}
			else
			{
				$this->session->set_flashdata('tr_msg', 'Record Saved Successfully');
				redirect('Restaurent');
			}
			// prd($this->input->post());
		}
		prepareview:

		if($hasValidationErrors){
			$content['hasValidationErrors']	=	true;
		}
		else
		{
			$content['hasValidationErrors']	=	false;				
		}

		$content['subview']   = 'addMenuItem';
		$this->load->view('main_layout',$content);
	}

	public function editItem($menu_id=NULL)
	{
		if(empty($menu_id))
		{
			$this->session->set_flashdata('er_msg', 'No item selected');
			redirect('Restaurent');
		}
		else
		{
			$RequestMethod       = $this->input->server('REQUEST_METHOD');
			$hasValidationErrors = false;

			if($RequestMethod == 'POST')
			{
				$menu_name        = $this->input->post('menu_name');
				$menu_description = $this->input->post('menu_description');
				$menu_type        = $this->input->post('menu_type');
				$menu_price       = $this->input->post('menu_price');

				$this->form_validation->set_rules('menu_name','Name','trim|required|alpha_numeric_spaces');
				$this->form_validation->set_rules('menu_description','Description','trim|required|alpha_numeric_spaces');
				$this->form_validation->set_rules('menu_type','Type','trim|required');
				$this->form_validation->set_rules('menu_price','Price','trim|required');
				if($this->form_validation->run() == FALSE)
				{
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible mt-1" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>',
						'</div>');

					$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

					$hasValidationErrors	=	true;
					goto prepareview;
				}
				$updateArray = array(
					'menu_name'        => $menu_name,
					'menu_description' => $menu_description,
					'menu_type'        => $menu_type,
					'menu_price'       => $menu_price,
					'menu_updatedOn'   => date('Y-m-d H:i:s'),
				);
				$updateData = $this->CommonModel->updateRecords('menu', $updateArray, array('menu_id' => $menu_id,));
				
				if($updateData)
				{
					$this->session->set_flashdata('tr_msg', 'Record Updated Successfully');
					redirect('Restaurent');
				}
				else
				{
					$this->session->set_flashdata('tr_msg', 'Connection Error. Please try later.');
					redirect('Restaurent');
				}
				// prd($this->input->post());
			}
			prepareview:

			if($hasValidationErrors){
				$content['hasValidationErrors']	=	true;
			}
			else
			{
				$content['hasValidationErrors']	=	false;				
			}

			$item               = $this->CommonModel->fetchRecords('menu', array('menu_id' => $menu_id, 'menu_delete' => 0));
			$content['item']    = $item;
			$content['subview'] = 'editItem';
			$this->load->view('main_layout',$content);
		}

	}

	public function deleteItem($menu_id=NULL)
	{
		if(empty($menu_id))
		{
			$this->session->set_flashdata('er_msg', 'No item selected');
			redirect('Restaurent');
		}
		else
		{
			$deleteItem = $this->CommonModel->softDelete('menu', array('menu_delete' => 1), array('menu_id' => $menu_id));
			redirect('Restaurent');
		}
	}

	public function logout()
	{
		// session_destroy();
		$this->session->unset_userdata('userLoginData');
		$this->session->set_flashdata('tr_msg', 'Logged out successfully');
		redirect(base_url('Site'));
	}
}
