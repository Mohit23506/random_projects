<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $userLoginData;
	public function __construct()
	{
		parent::__construct();
		$this->userLoginData = $this->session->userdata('userLoginData');
	}

	public function index()
	{
		$RequestMethod       = $this->input->server('REQUEST_METHOD');
		$hasValidationErrors = false;

		if($RequestMethod == 'POST')
		{
			// prd($this->input->post());
			$this->form_validation->set_rules('user_fname','First Name','trim|required|alpha');
			$this->form_validation->set_rules('user_address','Address','trim|required');
			$this->form_validation->set_rules('user_lname','Last Name','trim|alpha');

			// if email id is different from previous one, then only check for unique
			if($this->userLoginData->user_email != $this->input->post('user_email'))
			{
				$this->form_validation->set_rules('user_email','Email','trim|required|valid_email|is_unique[users.user_email]');
			}
			// if mobile no is different from previous one, then only check for unique
			if($this->userLoginData->user_mobile != $this->input->post('user_mobile'))
			{
				$this->form_validation->set_rules('user_mobile','Mobile','trim|required|is_unique[users.user_mobile]|exact_length[10]');
			}

			if($this->userLoginData->user_role < 1)
			{
				$this->form_validation->set_rules('user_preference','Preference','trim|required');
			}

			if($this->userLoginData->user_role > 0)
			{
				if($this->userLoginData->user_restaurantName != $this->input->post('user_restaurantName'))
				{
					$this->form_validation->set_rules('user_restaurantName','Restaurant Name','trim|required|is_unique[users.user_restaurantName]');
				}
			}

			$this->form_validation->set_rules('user_state','State','trim|required');
			$this->form_validation->set_rules('user_district','District','trim|required');

			if($this->form_validation->run() == FALSE)
			{
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible mt-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>',
					'</div>');

				$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors	=	true;
				goto prepareview;
			}
			$user_restaurantName = $this->input->post('user_restaurantName');
			// check if there is pic is uploaded
			$user_profilePic = $this->userLoginData->user_profilePic;

			if($_FILES['user_profilePic']['name'])
			{
				$fileStatus = do_upload($_FILES);
				// prd($fileStatus);
				if($fileStatus['status'] == 201)
				{
					$this->session->set_flashdata('er_msg', $fileStatus['data']);
					$hasValidationErrors	=	true;
					goto prepareview;
				}
				$user_profilePic = $fileStatus['data'];
			}

			$updateArr = array(
				'user_fname'          => trim($this->input->post('user_fname')),
				'user_lname'          => trim($this->input->post('user_lname')),
				'user_email'          => trim($this->input->post('user_email')),
				'user_restaurantName' => isset($user_restaurantName)?trim($user_restaurantName):'',
				'user_mobile'         => trim($this->input->post('user_mobile')),
				'user_address'        => trim($this->input->post('user_address')),
				'user_role'           => $this->userLoginData->user_role,
				'user_state'          => trim($this->input->post('user_state')),
				'user_district'       => trim($this->input->post('user_district')),
				'user_preference'     => trim($this->input->post('user_preference')),
				'user_profilePic'     => $user_profilePic,
				'user_createdOn'      => date('Y-m-d h:i:s'),
			);
			$this->CommonModel->updateRecords('users', $updateArr, array('user_id' => $this->userLoginData->user_id));
			// when data/record is updated then unset the session, and ask user to login
			$this->session->unset_userdata('userLoginData');
			$this->session->set_flashdata('tr_msg', 'Details Updated Successfully. Pleae login to see updated details.');
			redirect(base_url('Site/login'));
			// pr($_FILES['user_profilePic']);	prd($this->input->post());
		}

		prepareview:

		if($hasValidationErrors){
			$content['hasValidationErrors']	=	true;
		}
		else
		{
			$content['hasValidationErrors']	=	false;				
		}

		$userData            = $this->CommonModel->fetchRecords('users', array('user_id' => $this->userLoginData->user_id, 'user_role' => $this->userLoginData->user_role, 'user_delete' => 0));
		$content['userData'] = $userData[0];
		$content['userType'] = $this->userLoginData->user_role;
		$states              = $this->CommonModel->fetchRecords('states', array('is_deleted' => 0));
		$content['states']   = $states;
		$content['subview']  = 'profile';
		// prd($this->userLoginData);
		$this->load->view('main_layout',$content);
	}

	public function updatePassword()
	{
		$content['subview']  = 'updatePassword';
		$this->load->view('main_layout',$content);
	}
}
