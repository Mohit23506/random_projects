<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $userLoginData;
	public function __construct()
	{
		parent::__construct();
		$this->userLoginData = $this->session->userdata('userLoginData');
		if($this->session->userdata('userLoginData')->user_role == 1)
		{
			redirect('Restaurent');
		}
	}

	public function index($orderData=NULL, $restaurantId=NULL)
	{
		// fetching all the restaurents to show and order
		if($orderData)
		{
			$orderData = base64_decode($orderData);
			// echo $restaurantId.'<br>'; prd($orderData);
			$insertArray = array(
				'order_from'    => $this->userLoginData->user_id,
				'order_to'      => $restaurantId,
				'order_message' => $orderData,
			);
			$this->CommonModel->insert('orders', $insertArray);

			$this->session->set_flashdata('tr_msg', 'Order Placed Successfully');
			redirect('Customer/');
		}
		// $restaurants            = $this->CommonModel->fetchRecords('users', array('user_role' => 1, 'user_delete' => 0));
		$sql = "SELECT u.*, s.stateName, d.districtName FROM users u LEFT JOIN states s ON s.sid=u.user_state LEFT JOIN districts d ON d.did=u.user_district WHERE u.user_role=1 AND u.user_delete=0";
		$restaurants            = $this->CommonModel->executeQuery($sql);
		$content['restaurants'] = $restaurants;
		$content['subview']     = 'customerDashboard';
		$this->load->view('main_layout',$content);
	}

	public function logout()
	{
		// session_destroy();
		$this->session->unset_userdata('userLoginData');
		$this->session->set_flashdata('tr_msg', 'Logged out successfully');
		redirect(base_url('Site'));
	}
}
