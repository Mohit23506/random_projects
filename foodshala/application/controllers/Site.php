<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if($this->session->userdata('userLoginData') != NULL)
		{
			if($this->session->userdata('userLoginData')->user_role == 1)
			{
				redirect('Restaurent');
			}
			else
			{
				redirect('Customer');
			}
		}
	}

	public function index()
	{
		$sql = "SELECT u.*, s.stateName, d.districtName FROM users u LEFT JOIN states s ON s.sid=u.user_state LEFT JOIN districts d ON d.did=u.user_district WHERE u.user_role=1 AND u.user_delete=0";
		// $restaurants = $this->CommonModel->fetchRecords('users', array('user_role' => 1, 'user_delete' => 0));
		$restaurants            = $this->CommonModel->executeQuery($sql);
		$content['restaurants'] = $restaurants;
		$content['subview']     = 'site';
		$this->load->view('main_layout',$content);
	}

	public function login($orderData=NULL, $restaurantId=NULL)
	{
		// echo 'login here, create single login page';
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			$user_email    = $this->input->post('user_email');
			$user_password = $this->input->post('user_password');
			if (empty($user_email) || empty($user_password))
			{
				$this->session->set_flashdata('er_msg', 'Please provide email and password.');
				redirect(base_url('Site/login/'));
			}

			$where = array(
				'user_email'    => trim($user_email),
				'user_password' => md5(trim($user_password)),
				'user_delete'   => 0,
			);
			$result = $this->CommonModel->fetchRecords('users', $where, 'user_id, user_fname, user_lname, user_email, user_restaurantName, user_mobile, user_role, user_preference, user_profilePic, user_lastLogin');
			if(count($result) > 0)
			{
				// update last login field
				$updateLastLogin = $this->CommonModel->updateRecords('users', array('user_lastLogin' => date('Y-m-d h:i:s')), array('user_id' => $result[0]->user_id));
				if(empty($result[0]->user_lastLogin))
				{
					$result[0]->user_lastLogin = date('d-m-Y h:i:s',time());
				}
				$this->session->set_userdata('userLoginData',$result[0]);
				$this->session->set_flashdata('tr_msg', 'Logged In Successfully.');
				if($this->session->userdata('userLoginData')->user_role == 1)
				{
					redirect('Restaurent/index/'.$orderData.'/'.$restaurantId);
				}
				else
				{
					redirect('Customer/index/'.$orderData.'/'.$restaurantId);
				}
			}
			else
			{
				$this->session->set_flashdata('er_msg', 'Invalid Credentials.');
				redirect(base_url('Site/login/'));
			}
		}

		$content['subview'] = 'login';
		$this->load->view('main_layout',$content);
	}

	public function register($type=NULL)
	{
		$RequestMethod       = $this->input->server('REQUEST_METHOD');
		$hasValidationErrors = false;

		if($RequestMethod == 'POST')
		{
			// prd($this->input->post());
			$this->form_validation->set_rules('user_fname','First Name','trim|required|alpha');
			$this->form_validation->set_rules('user_address','Address','trim|required');
			$this->form_validation->set_rules('user_lname','Last Name','trim|alpha');
			$this->form_validation->set_rules('user_email','Email','trim|required|valid_email|is_unique[users.user_email]');
			$this->form_validation->set_rules('user_mobile','Mobile','trim|required|is_unique[users.user_mobile]|exact_length[10]');
			if($this->input->post('user_role') < 1)
			{
				$this->form_validation->set_rules('user_preference','Preference','trim|required');
			}

			if($this->input->post('user_role') > 0)
			{
				$this->form_validation->set_rules('user_restaurantName','Restaurant Name','trim|required|is_unique[users.user_restaurantName]');
			}

			$this->form_validation->set_rules('user_state','State','trim|required');
			$this->form_validation->set_rules('user_district','District','trim|required');
			$this->form_validation->set_rules('user_password','Password','trim|required');
			$this->form_validation->set_rules('repeat_password','Repeat Password','trim|required|matches[user_password]');
			if($this->form_validation->run() == FALSE)
			{
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>',
					'</div>');

				$this->session->set_flashdata('er_msg', 'There have been validation error(s), please check the error messages');

				$hasValidationErrors	=	true;
				goto prepareview;
			}
			$user_restaurantName = $this->input->post('user_restaurantName');
			// check if there is pic is uploaded
			$user_profilePic = '';
			if($_FILES['user_profilePic']['name'])
			{
				$fileStatus = do_upload($_FILES);
				// prd($fileStatus);
				if($fileStatus['status'] == 201)
				{
					$this->session->set_flashdata('er_msg', $fileStatus['data']);
					$hasValidationErrors	=	true;
					goto prepareview;
				}
				$user_profilePic = $fileStatus['data'];
			}
			$insertArr = array(
				'user_fname'          => trim($this->input->post('user_fname')),
				'user_lname'          => trim($this->input->post('user_lname')),
				'user_email'          => trim($this->input->post('user_email')),
				'user_restaurantName' => isset($user_restaurantName)?trim($user_restaurantName):'',
				'user_mobile'         => trim($this->input->post('user_mobile')),
				'user_address'        => trim($this->input->post('user_address')),
				'user_password'       => md5(trim($this->input->post('user_password'))),
				'user_role'           => $this->input->post('user_role'),
				'user_state'          => trim($this->input->post('user_state')),
				'user_district'       => trim($this->input->post('user_district')),
				'user_preference'     => trim($this->input->post('user_preference')),
				'user_profilePic'     => $user_profilePic,
				'user_createdOn'      => date('Y-m-d h:i:s'),
			);
			$this->CommonModel->insert('users', $insertArr);

			$this->session->set_flashdata('tr_msg', 'Registration Successfull');
			redirect(base_url('Site/login/'));
			// pr($_FILES['user_profilePic']);	prd($this->input->post());
		}

		prepareview:

		if($hasValidationErrors){
			$content['hasValidationErrors']	=	true;
		}
		else
		{
			$content['hasValidationErrors']	=	false;				
		}

		$states              = $this->CommonModel->fetchRecords('states', array('is_deleted' => 0));
		$content['states']   = $states;
		$content['userType'] = $type;
		$content['subview']  = 'register';
		$this->load->view('main_layout',$content);
	}

	public function resetPassword()
	{
		$content['subview'] = 'resetPassword';
		$this->load->view('main_layout',$content);
	}
}
