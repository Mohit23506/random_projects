<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $userLoginData;
	public function __construct()
	{
		parent::__construct();
		$this->userLoginData = $this->session->userdata('userLoginData');
	}

	public function index($orderData=NULL, $restaurantId=NULL)
	{
		if($this->userLoginData->user_role == 1)
		{
			$orderData = $this->CommonModel->getMenuItems('orders', 'users', 'orders.*,concat(users.user_fname," ", users.user_lname) AS fullName,users.user_email, users.user_address','users.user_id=orders.order_from',"order_to=".$this->userLoginData->user_id." AND order_deleteTo=0", 'orders.order_createdOn DESC');
			// prd($orderData);
			$content['orderData'] = $orderData;
			$content['subview']   = 'restaurentOrders';
		}
		else
		{
			$orderData = $this->CommonModel->getMenuItems('orders', 'users', 'orders.*,users.user_restaurantName','users.user_id=orders.order_to',"order_from=".$this->userLoginData->user_id." AND order_deleteFrom=0", 'orders.order_createdOn DESC');
			// prd($orderData);
			$content['orderData'] = $orderData;
			$content['subview']   = 'customerOrders';
		}

		$this->load->view('main_layout',$content);
	}

	public function delete($order_id=NULL, $order_status=NULL)
	{
		if(empty($order_id))
		{
			$this->session->set_flashdata('er_msg', 'No Order selected');
			redirect('Orders');
		}
		else
		{
			if($this->userLoginData->user_role == 1)
			{
				// for restaurents
				if($order_status < 1)
				{
					$this->session->set_flashdata('er_msg', 'Pending order (Accept or Reject first). So, can not be deleted.');
					redirect('Orders');
				}
				$deleteItem = $this->CommonModel->softDelete('orders', array('order_deleteTo' => 1), array('order_id' => $order_id));
			}
			else
			{
				// for customers
				// if order is pending and user want to delete then this will be completed deleted for both, so that restaurant could not see this
				if($order_status < 1)
				{
					$deleteItem = $this->CommonModel->deleteRecords('orders', array('order_id' => $order_id));
					$this->session->set_flashdata('tr_msg', 'Record Deleted Successfully.');
				}
				else
				{
					$deleteItem = $this->CommonModel->softDelete('orders', array('order_deleteFrom' => 1), array('order_id' => $order_id));
				}
			}
			redirect('Orders');
		}
	}

	public function action($order_id=NULL,$action=NULL)
	{
		if($this->userLoginData->user_role < 1)
		{
			// only restaurant can accept and reject the orders
			$this->session->set_flashdata('er_msg', 'You are not allowed to access that page.');
			redirect('Orders');
		}
		$arr = array('accept','reject');
		if(empty($order_id) || !in_array($action, $arr))
		{
			$this->session->set_flashdata('er_msg', 'Please provide valid order number and action type.');
			redirect('Orders');
		}
		else
		{
			if($action == 'accept')
			{
				$accept = $this->CommonModel->softDelete('orders', array('order_status' => 1), array('order_id' => $order_id));
				if($accept != 'not_exist')
				{
					// write code to send notification email to customer regarding his/her order
					$this->session->set_flashdata('tr_msg', 'Order Accepted Successfully.');
				}
				redirect('Orders');
			}

			if($action == 'reject')
			{
				$reject = $this->CommonModel->softDelete('orders', array('order_status' => 2), array('order_id' => $order_id));
				if($reject != 'not_exist')
				{
					// write code to send notification email to customer regarding his/her order
					$this->session->set_flashdata('tr_msg', 'Order Rejected Successfully.');
				}
				redirect('Orders');
			}
		}
	}
}
