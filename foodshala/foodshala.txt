user_fname
user_lname
user_userName
user_email
user_mobile
user_password
user_state
user_district
user_preference
<!-- -- add below field only if registering for restaurant users/owners -->
user_restaurantName


The application should contain 2 types of users: Restaurants and Customers //// DONE

‘Registration’ pages - Different registration pages for Restaurants & Customers. Capture customer’s preferences (veg/non-veg) during registration. //// DONE

‘Login’ pages - Single/different login pages for restaurants & customers. //// DONE

‘Add menu item’ page - A restaurant, once logged in, should be able to add details of new food items (including whether they are veg or non-veg) to their restaurant’s menu. Access to this page should be restricted only to restaurants.

‘Menu’ page - There should be a page that displays all the available food items along with which restaurants have them and a ‘Order’ button. This page should be accessible to everyone, irrespective of whether the user is logged in or not. Expected functionality on click of the 'Order' button-  

- Only customers should be able to order food by clicking the ‘Order’ button.
- It’s optional to implement cart functionality.
- If the user is not logged in, then he/she should be redirected to the login page.
- If a user is logged in as a restaurant, then the user should not be allowed to order the food.

‘View orders’ page - Restaurant should be able to see the list of all the customers who have ordered from their restaurant along with the food items they have ordered.





1. create seprate controller for login and redirect to appropiate dashboard
2. create seprate controller for registration and save data accordingly

// both the above controller will extend ci_controller, and if login session data is null then it will be called/function/work


3. create restaurent dashboard and show all it's data here
4. create customer dashboard and show all it's data with the listing of restaurents
5. create notification function (try to do this vai ajax)
6. create a landing page listing of all the restaurents with all it's menus to order, if logged in then only allow to order, if not then login first and then place orders.