import React from "react";

{/*This is using JSX*/}
{/*
  return(
    <div>
      <h4>This is and jsx Component</h4>
    </div>
  )
*/}
{/*This is using without JSX*/}
const CompJsx = () => {
  return(
    React.createElement('div',
    {className:'parendDiv'},
    React.createElement('h4',
    {id:'firstId',className:'wonder'},
    'This is first component using React.createElement')
    )
  )
}

export default CompJsx