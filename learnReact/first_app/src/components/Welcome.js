import React, {Component} from "react";

class Welcome extends Component{
  render(){
    return(
      <div>
        <h4>This is first ES6 Class component</h4>
      </div>
    )
  }
}

export default Welcome