import React from "react";

{/*
  // this is the default function style in js
  function Greet() {
  return(
    <div>
      <h4>This is first Functional component</h4>
    </div>
  )
}
*/}

{/*
  // writing the function using ES6 like JSX in new form, there are two types in which we can write functions
*/}

{/*
  This is name export
  so we will import like this:- import {Greet} from './components/Greet'
  export const Greet = () =>  <h4>This is ES6 function type component</h4>
*/}

{/*
  This is default export
*/}
  const Greet = () => {
  return(
    <h4>This is ES6 function type component</h4>
  )
}

export default Greet
