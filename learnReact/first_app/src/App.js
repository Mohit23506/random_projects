import React from 'react';
import Greet from './components/Greet'
import Welcome from './components/Welcome'
import CompJsx from './components/CompJsx'

import './App.css';

function App() {
    return (
        <div className="App">
					<h1>This is my home page from App.JS</h1>
					<Greet />
					<Welcome />
					<CompJsx />
				</div>
    )
}

export default App;